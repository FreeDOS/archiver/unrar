# UnRAR

Unrar is a file decompressor with high compression ratio.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## UNRAR.LSM

<table>
<tr><td>title</td><td>UnRAR</td></tr>
<tr><td>version</td><td>3.80a</td></tr>
<tr><td>entered&nbsp;date</td><td>2012-09-25</td></tr>
<tr><td>description</td><td>A file decompressor with a high compression ratio</td></tr>
<tr><td>keywords</td><td>Unrar, rar, decompressor, archive</td></tr>
<tr><td>author</td><td>Alexander L. Roshal</td></tr>
<tr><td>maintained&nbsp;by</td><td>Robert Riebisch and others</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.rarlab.com</td></tr>
<tr><td>primary&nbsp;site</td><td>http://www.bttr-software.de/ports/#unrar</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Freeware, see license.txt</td></tr>
<tr><td>summary</td><td>Unrar is a file decompressor with high compression ratio.</td></tr>
</table>
